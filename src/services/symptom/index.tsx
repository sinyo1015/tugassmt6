import { API } from '../../config';
import { setHospitals, setSymptom, setSymptoms, store } from '../../libs';
import { handleAsync } from '../../utils';

const { dispatch } = store;

export const getSymptoms = async (payload = {}) => {
  const [res, err] = await handleAsync(
    API.base.getGejala({
      params: payload,
    }),
  );
  if (err) throw err;
  dispatch(setSymptoms(res?.data?.gejala));
};
