import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import React from 'react';
import {
  Dashboard,
  Splash,
  Search,
  SelfAssessment,
  ResultAssessment,
  Hospital,
} from '../screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomTabNavigator } from '../components';

export type ScreenParamList = {
  Splash: undefined;
  Dashboard: undefined;
  SignIn: undefined;
  Hospital: undefined;
  Search: undefined;
  SelfAssessment: undefined;
  ResultAssessment: undefined;
  SignUp: undefined;
  Consultation: undefined;
};
const Stack = createStackNavigator<ScreenParamList>();

const forFade = ({ current }: { current: any }) => ({
  cardStyle: {
    opacity: current.progress.interpolate({
      inputRange: [0, 0.5, 0.9, 1],
      outputRange: [0, 0.25, 0.7, 1],
    }),
  },
});


const Router = () => (
  <Stack.Navigator initialRouteName="Splash">
    <Stack.Screen
      name="Splash"
      component={Splash}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Dashboard"
      component={Dashboard}
      options={{
        headerShown: false,
        cardStyleInterpolator: forFade,
      }}
    />
    <Stack.Screen
      name="Hospital"
      component={Hospital}
      options={{
        headerShown: false,
        cardStyleInterpolator: forFade,
      }}
    />
    <Stack.Screen
      name="Search"
      component={Search}
      options={{
        headerShown: false,
        cardStyleInterpolator: forFade,
      }}
    />
    <Stack.Screen
      name="SelfAssessment"
      component={SelfAssessment}
      options={{
        headerShown: false,
        cardStyleInterpolator:
          CardStyleInterpolators.forRevealFromBottomAndroid,
      }}
    />
    <Stack.Screen
      name="ResultAssessment"
      component={ResultAssessment}
      options={{
        headerShown: false,
        cardStyleInterpolator: forFade,
      }}
    />
  </Stack.Navigator>
);

export default Router;
