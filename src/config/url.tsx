import appConfig from './constant';

export const config = appConfig;

interface BaseUrl {
  gejala?: any;
  vaccination?: any;
  hospitals?: any;
  assessment?: any;
}

const baseUrl: BaseUrl = {
  gejala: `${config.url.api_sikarma}/gejala`,
  vaccination: `${config.url.api}/vaccination/current`,
  hospitals: `${config.url.api}/hospitals/list`,
  assessment: `${config.url.api_sikarma}/assessment`,
};

export default baseUrl;
