import Dashboard from './Dashboard';
import Splash from './Splash';
import Search from './Search';
import SelfAssessment from './SelfAssessment';
import ResultAssessment from './ResultAssessment';
import Hospital from './Hospital';
export {
  Splash,
  Dashboard,
  Search,
  SelfAssessment,
  ResultAssessment,
  Hospital,
};
